﻿namespace CRUDBASIC3
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBoxNomes = new System.Windows.Forms.ComboBox();
            this.comboBoxListaDeAlunos = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonProcurar = new System.Windows.Forms.Button();
            this.textBoxProcura = new System.Windows.Forms.TextBox();
            this.comboBoxProcura = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBoxPrimeiroNome = new System.Windows.Forms.TextBox();
            this.TextBoxIdAluno = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TextBoxApelido = new System.Windows.Forms.TextBox();
            this.ButtonCanselar = new System.Windows.Forms.Button();
            this.ButtonUpdate = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.comboBoxNomes);
            this.groupBox1.Controls.Add(this.comboBoxListaDeAlunos);
            this.groupBox1.Location = new System.Drawing.Point(44, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(462, 569);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Listagens";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TextBoxIdAluno);
            this.groupBox2.Controls.Add(this.TextBoxPrimeiroNome);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(6, 173);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(737, 96);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Éditar";
            // 
            // comboBoxNomes
            // 
            this.comboBoxNomes.FormattingEnabled = true;
            this.comboBoxNomes.Location = new System.Drawing.Point(6, 87);
            this.comboBoxNomes.Name = "comboBoxNomes";
            this.comboBoxNomes.Size = new System.Drawing.Size(304, 21);
            this.comboBoxNomes.TabIndex = 1;
            // 
            // comboBoxListaDeAlunos
            // 
            this.comboBoxListaDeAlunos.FormattingEnabled = true;
            this.comboBoxListaDeAlunos.Location = new System.Drawing.Point(6, 29);
            this.comboBoxListaDeAlunos.Name = "comboBoxListaDeAlunos";
            this.comboBoxListaDeAlunos.Size = new System.Drawing.Size(304, 21);
            this.comboBoxListaDeAlunos.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonProcurar);
            this.groupBox3.Controls.Add(this.textBoxProcura);
            this.groupBox3.Controls.Add(this.comboBoxProcura);
            this.groupBox3.Location = new System.Drawing.Point(525, 21);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(279, 145);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Procurar";
            // 
            // buttonProcurar
            // 
            this.buttonProcurar.Location = new System.Drawing.Point(88, 87);
            this.buttonProcurar.Name = "buttonProcurar";
            this.buttonProcurar.Size = new System.Drawing.Size(87, 21);
            this.buttonProcurar.TabIndex = 2;
            this.buttonProcurar.Text = "Procurar";
            this.buttonProcurar.UseVisualStyleBackColor = true;
            this.buttonProcurar.Click += new System.EventHandler(this.buttonProcurar_Click);
            // 
            // textBoxProcura
            // 
            this.textBoxProcura.Location = new System.Drawing.Point(132, 40);
            this.textBoxProcura.Name = "textBoxProcura";
            this.textBoxProcura.Size = new System.Drawing.Size(130, 20);
            this.textBoxProcura.TabIndex = 1;
            // 
            // comboBoxProcura
            // 
            this.comboBoxProcura.FormattingEnabled = true;
            this.comboBoxProcura.Location = new System.Drawing.Point(6, 40);
            this.comboBoxProcura.Name = "comboBoxProcura";
            this.comboBoxProcura.Size = new System.Drawing.Size(98, 21);
            this.comboBoxProcura.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "IdAluno:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(227, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Primeiro nome:";
            // 
            // TextBoxPrimeiroNome
            // 
            this.TextBoxPrimeiroNome.Location = new System.Drawing.Point(309, 23);
            this.TextBoxPrimeiroNome.Name = "TextBoxPrimeiroNome";
            this.TextBoxPrimeiroNome.Size = new System.Drawing.Size(128, 20);
            this.TextBoxPrimeiroNome.TabIndex = 2;
            // 
            // TextBoxIdAluno
            // 
            this.TextBoxIdAluno.Location = new System.Drawing.Point(102, 26);
            this.TextBoxIdAluno.Name = "TextBoxIdAluno";
            this.TextBoxIdAluno.Size = new System.Drawing.Size(99, 20);
            this.TextBoxIdAluno.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(528, 223);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Apelido:";
            // 
            // TextBoxApelido
            // 
            this.TextBoxApelido.Location = new System.Drawing.Point(576, 220);
            this.TextBoxApelido.Name = "TextBoxApelido";
            this.TextBoxApelido.Size = new System.Drawing.Size(128, 20);
            this.TextBoxApelido.TabIndex = 5;
            // 
            // ButtonCanselar
            // 
            this.ButtonCanselar.Enabled = false;
            this.ButtonCanselar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ButtonCanselar.Image = global::CRUDBASIC3.Properties.Resources.if_Delete_1493279;
            this.ButtonCanselar.Location = new System.Drawing.Point(588, 246);
            this.ButtonCanselar.Name = "ButtonCanselar";
            this.ButtonCanselar.Size = new System.Drawing.Size(70, 38);
            this.ButtonCanselar.TabIndex = 7;
            this.ButtonCanselar.UseVisualStyleBackColor = true;
            this.ButtonCanselar.Click += new System.EventHandler(this.ButtonCanselar_Click);
            // 
            // ButtonUpdate
            // 
            this.ButtonUpdate.Enabled = false;
            this.ButtonUpdate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ButtonUpdate.Image = global::CRUDBASIC3.Properties.Resources.if_cloud_arrow_up_532264;
            this.ButtonUpdate.Location = new System.Drawing.Point(512, 246);
            this.ButtonUpdate.Name = "ButtonUpdate";
            this.ButtonUpdate.Size = new System.Drawing.Size(70, 38);
            this.ButtonUpdate.TabIndex = 6;
            this.ButtonUpdate.UseVisualStyleBackColor = true;
            this.ButtonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 626);
            this.Controls.Add(this.ButtonCanselar);
            this.Controls.Add(this.ButtonUpdate);
            this.Controls.Add(this.TextBoxApelido);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBoxNomes;
        private System.Windows.Forms.ComboBox comboBoxListaDeAlunos;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonProcurar;
        private System.Windows.Forms.TextBox textBoxProcura;
        private System.Windows.Forms.ComboBox comboBoxProcura;
        private System.Windows.Forms.TextBox TextBoxIdAluno;
        private System.Windows.Forms.TextBox TextBoxPrimeiroNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBoxApelido;
        private System.Windows.Forms.Button ButtonUpdate;
        private System.Windows.Forms.Button ButtonCanselar;
    }
}

