﻿namespace CRUDBASIC3
{
    using CRUDBasico.Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;


    public partial class Form1 : Form
    {
        List<Aluno> ListaDeAlunos;

        public Form1()
        {
            InitializeComponent();

            ListaDeAlunos = new List<Aluno>();

            ListaDeAlunos.Add(new Aluno() { IdAluno = 19, PrimeiroNome = "Ruben", Apelido = "Gouveia" });
            ListaDeAlunos.Add(new Aluno() { IdAluno = 12, PrimeiroNome = "Gustavo", Apelido = "Nogueira" });
            ListaDeAlunos.Add(new Aluno() { IdAluno = 13, PrimeiroNome = "Igor", Apelido = "Vasylkiv" });
            ListaDeAlunos.Add(new Aluno() { IdAluno = 20, PrimeiroNome = "Rui", Apelido = "Rodrigues" });
            ListaDeAlunos.Add(new Aluno() { IdAluno = 11, PrimeiroNome = "Guilherme", Apelido = "Oliveira" });
            ListaDeAlunos.Add(new Aluno() { IdAluno = 10, PrimeiroNome = "Luis", Apelido = "Teixeira" });
            ListaDeAlunos.Add(new Aluno() { IdAluno = 15, PrimeiroNome = "Nilton", Apelido = "Borges" });

            comboBoxListaDeAlunos.DataSource = ListaDeAlunos;
            comboBoxProcura.DataSource = new List<string>()
            {
                "IdAluno", "Apelido"
            };

            foreach (var aluno in ListaDeAlunos)
            {
                comboBoxNomes.Items.Add(aluno.NomeCompleto);
            }


        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonProcurar_Click(object sender, EventArgs e)
        {
            if (comboBoxProcura.SelectedIndex == -1)
            {
                MessageBox.Show("Escolha um critério!!");
                return;
            }

            if (String.IsNullOrEmpty(textBoxProcura.Text))
            {
                MessageBox.Show("Acaixa de texto náo pode estar aqui");
                return;
            }


            int id = 0;

            if (comboBoxProcura.SelectedIndex == 0 && !Int32.TryParse(textBoxProcura.Text, out id))
            {
                MessageBox.Show("O id do aluno tem de ser numerico");
                return;
            }

            Aluno alunoAchado;

            foreach (var aluno in ListaDeAlunos)

            {
                if (id == aluno.IdAluno)
                {
                    // MessageBox.Show(aluno.NomeCompleto);

                    TextBoxIdAluno.Text = aluno.IdAluno.ToString();
                    TextBoxPrimeiroNome.Text = aluno.PrimeiroNome;
                    TextBoxApelido.Text = aluno.Apelido;

                    ButtonUpdate.Enabled = true;
                    ButtonCanselar.Enabled = true;


                    return;
                }

            }
            String apelido = TextBoxApelido.Text;
            if (comboBoxProcura.SelectedIndex ==1 )
            {
                foreach (var aluno in ListaDeAlunos)
                {

                    if (apelido == aluno.Apelido)
                    {
                        TextBoxIdAluno.Text = aluno.IdAluno.ToString();
                        TextBoxPrimeiroNome.Text = aluno.PrimeiroNome;
                        TextBoxApelido.Text = aluno.Apelido;


                        alunoAchado = Aluno;


                    }




                }
                ButtonUpdate.Enabled = true;
                ButtonCanselar.Enabled = true;
                return;
            }
            

        }

        private void ButtonCanselar_Click(object sender, EventArgs e)
        {
            TextBoxIdAluno.Text = string.Empty;
            TextBoxPrimeiroNome.Text = string.Empty;
            TextBoxApelido.Text = string.Empty;

            ButtonUpdate.Enabled = false;
            ButtonCanselar.Enabled = false;
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            alunoAchado.IdAluno = Convert.ToInt32(TextBoxIdAluno.Text);
            alunoAchado.PrimeiroNome = TextBoxPrimeiroNome.Text;
            alunoAchado.Apelido = TextBoxApelido.Text;

            alunoAchado = null;

            private void LimpaEditar()
            {

                TextBoxIdAluno.Text = string.Empty;
                TextBoxPrimeiroNome.Text = string.Empty;
                TextBoxApelido.Text = string.Empty;
            }
            
        }
    }
}
